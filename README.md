# Excel

SPA Excel app with native javascript.

### Web app
See [DEMO](https://sudakoff.gitlab.io/excel/).

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```
