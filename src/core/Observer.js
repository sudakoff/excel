export class Observer {
  constructor() {
    this.listeners = {}
  }
  // Уведомление слушателей
  // table.dispatch('table:select', {a: 1})
  dispatch(event, ...args) {
    if (!Array.isArray(this.listeners[event])) {
      return false
    }
    this.listeners[event].forEach((listener) => {
      listener(...args)
    })
    return true
  }
  // Подписка на уведомления, добавление нового слушателя
  // formula.subscribe('table:select', () => {})
  subscribe(event, fn) {
    this.listeners[event] = this.listeners[event] || []
    this.listeners[event].push(fn)
    return () => {
      this.listeners[event] =
        this.listeners[event].filter((listener) => listener !== fn)
    }
  }
}


// const emitter = new Observer()

// emitter.subscribe('First', (data) => console.log('Go', data))
// emitter.dispatch('Bla-bla', 454)
